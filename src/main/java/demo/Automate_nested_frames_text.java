package demo;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
//Selenium Imports
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
///


public class Automate_nested_frames_text {
    ChromeDriver driver;
    public Automate_nested_frames_text()
    {
        System.out.println("Constructor: TestCases");
        WebDriverManager.chromedriver().timeout(30).setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    public void endTest()
    {
        System.out.println("End Test: TestCases");
        driver.close();
        driver.quit();

    }


    public  void testCase01() throws InterruptedException {
        System.out.println("Start Test case: testCase01");

        driver.get("https://the-internet.herokuapp.com/nested_frames");
        driver.switchTo().frame(0);
        for(int i=0; i<3; i++){
            driver.switchTo().frame(i);
            WebElement FirstFrameText = driver.findElement(By.xpath("//html/body"));
            System.out.println(FirstFrameText.getText());
            driver.switchTo().parentFrame();
        }
        driver.switchTo().parentFrame();
        driver.switchTo().frame(1);
        WebElement FirstFrameText = driver.findElement(By.xpath("//html/body"));
        System.out.println(FirstFrameText.getText());

        System.out.println("end Test case: testCase02");
    }


}
